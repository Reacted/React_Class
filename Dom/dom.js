let listone = document.getElementsByClassName("listone");
let all = document.querySelectorAll('UL');

// The ES6 way
let allList = [...document.querySelectorAll('ul')];

let keys,
  keysArray,
  value,
  valueArray,
  newObject = {};

for (let i = 0; i < all.length; i++) {
  if (all[i].className === "listone") {
    keys = all[i].textContent;
  } else if (all[i].nodeName === "UL") {
    value = all[i].textContent;
  }
  //   The magic is here
  newObject[keys] = value;
}

// document.write(JSON.stringify(newObject));
console.log(newObject);