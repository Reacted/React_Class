import React, { Component } from "react";
import logo from "../logo.svg";
import "../styles/App.css";
// import Content from './Content';
import Table from "./Table";
import ToggleButton from "./ToggleButton";

class App extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     data: 0
  //   };
  // }

  // setNewNumber = () => {
  //   this.setState({ data: this.state.data + 1 });
  // };

  state = {
    person: [
      {
        id: 1,
        name: "Fayvor George",
        email: "fayvorgeorge@outlook.com"
      },
      {
        id: 2,
        name: "chidera Paul",
        email: "dexious@gmail.com"
      },
      {
        id: 3,
        name: "Zoe Oduduabasi",
        email: "smartzoe@gmail.com"
      },
      {
        id: 4,
        name: "Chilaka Obinna",
        email: "lackajs@gmail.com"
      },
      {
        id: 5,
        name: "Rutherford Rudy",
        email: "ruddyv@gmail.com"
      },
      {
        id: 6,
        name: "Grace Onoja",
        email: "grace@gmail.com"
      },
      {
        id: 7,
        name: "Bernadine Eze",
        email: "demayous@gmail.com"
      },
      {
        id: 8,
        name: "Chibuike Vincent",
        email: "chibuikevin@gmail.com"
      },
      {
        id: 9,
        name: "Emmanuel Raymond",
        email: "peoray@gmail.com"
      },
      {
        id: 10,
        name: "Shalom Mathew",
        email: "shallymat@gmail.com"
      },
      {
        id: 11,
        name: "Mac Anthony",
        email: "macanthony@gmail.com"
      },
      {
        id: 12,
        name: "Ezekiel Andrew",
        email: "ezekielblanc@gmail.com"
      }
    ]
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Let's learn React</h1>
        </header>
        <ToggleButton />
        <Table persons={this.state.person} />
      </div>
    );
  }
}

export default App;
