import React, { Component } from "react";

export default class Thead extends Component {
  render() {
    return (
      <thead className='thead'>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>EMAIL</th>
        </tr>
      </thead>
    );
  }
}
