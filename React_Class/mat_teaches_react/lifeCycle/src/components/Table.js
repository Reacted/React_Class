import React, { Component } from "react";
import Thead from "./Thead";
import Tbody from "./Tbody";

export default class Table extends Component {
    render() {
        const { persons } = this.props;
        const person = persons.map(person => (
            <Tbody person = { person } key = { person.id } />))
    return (
      <table border="1" className="table">
            <Thead />

            { person }
      </table>
    );
  }
}
