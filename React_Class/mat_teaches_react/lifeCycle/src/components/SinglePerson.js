import React from 'react'

const SinglePerson = (props) => (
    <div>
        <p><strong>Id:</strong>{props.person.id}</p>
        <p><strong>Name:</strong>{props.person.name}</p>
        <p><strong>Email:</strong>{props.person.email}</p>
    </div>
);

export default SinglePerson