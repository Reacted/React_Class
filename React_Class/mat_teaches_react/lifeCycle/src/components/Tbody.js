// import React, { Component } from "react";
import React from "react";

const Tbody = (props) =>
(  <tbody>
    <tr>
      <td>{props.person.id}</td>
      <td>{props.person.name}</td>
      <td>{props.person.email}</td>
    </tr>
  </tbody>
);

export default Tbody;
