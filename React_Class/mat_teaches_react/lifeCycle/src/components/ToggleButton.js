import React, { Component } from "react";

export default class ToggleButton extends Component {
  state = {
    clicked: false
  };

  handleClick = () => {
    this.setState({
      clicked: !this.state.clicked
    });
  };

  handleButton = () => {
    if (this.state.clicked) {
        return <button style={styles.buttonLogout} onClick={this.handleClick}>Logout</button>;
    } else {
        return <button style={styles.buttonLogin} onClick={this.handleClick}>Login</button>;
    }
  };

  render() {
    return <div style={styles.body}>{this.handleButton()}</div>;
  }
}

const styles = {
  buttonLogin: {
    width: "12rem",
      backgroundColor: "rgb(16, 68, 62)",
    color: "#f9f9f9",
    padding: '1rem',
    border: 'none',
    fontSize: '1.2rem',
    cursor: 'pointer'
  },
  body: {
    marginTop: "1rem",
    marginBottom: "3rem"
    },
  buttonLogout: {
    width: "12rem",
      backgroundColor: "#e41b1b",
    color: "#f9f9f9",
    padding: '1rem',
    border: 'none',
    fontSize: '1.2rem',
    cursor: 'pointer'
  }
};
